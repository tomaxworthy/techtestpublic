﻿using System.Web.Http.Results;
using System.Web.Mvc;
using RestSharp;
using TA.TechTest.Models.ViewModels;
using TA.TechTest.RestTools;
using TA.TechTest.Web.Configuration;

namespace TA.TechTest.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRestClient _restClient;
        private readonly IConfiguration _configuration;

        public HomeController(IRestClient restClient, IConfiguration configuration)
        {
            _restClient = restClient;
            _configuration = configuration;
        }
        public ActionResult Index()
        {
            var customerInvoiceDataRequest = new JsonRestRequest(_configuration.BusinessApiCustomerInvoiceDataPath, Method.GET);

            var customerInvoiceDataResponse = _restClient.Execute<CustomerInvoiceViewModel>(customerInvoiceDataRequest);

            if ((customerInvoiceDataResponse.Data) == null)
                return View(new CustomerInvoiceViewModel());

            return View(customerInvoiceDataResponse.Data);
        }

        public JsonResult GetCustomerInfo(string customerId)
        {
            if (customerId == null)
            {
                return new JsonResult();
            }

            var integerCustomerId = int.Parse(customerId);
            
            var customerDataRequest = new JsonRestRequest(_configuration.BusinessApiGetCustomerPath(integerCustomerId), Method.GET);

            var customerDataResponse = _restClient.Execute<CustomerViewModel>(customerDataRequest);

            return Json(customerDataResponse.Data, JsonRequestBehavior.AllowGet);
        }
    }
}