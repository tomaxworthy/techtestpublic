﻿using RestSharp;
using StructureMap;
using TA.TechTest.Web.Configuration;

namespace TA.TechTest.Web.IoC {
    public class WebRegistry : Registry {
        public WebRegistry() {
            Configure();
        }

        public WebRegistry(IConfiguration configuration)
        {
            For<IRestClient>().Use(new RestClient(configuration.BusinessApiBaseUrl));
            Configure();
        }

        private void Configure() {
            Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.WithDefaultConventions();
            });
        }

    }
}