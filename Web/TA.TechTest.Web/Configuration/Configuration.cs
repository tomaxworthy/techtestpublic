﻿using System.Configuration;

namespace TA.TechTest.Web.Configuration
{
    public class Configuration : IConfiguration
    {
        public string BusinessApiBaseUrl { get { return ConfigurationManager.AppSettings["BusinessApiBaseUrl"]; } }
        public string BusinessApiCustomerInvoiceDataPath { get { return ConfigurationManager.AppSettings["BusinessApiCustomerInvoiceDataPath"]; } }
        public string BusinessApiGetCustomerPath(int customerId) { return string.Format(ConfigurationManager.AppSettings["BusinessApiGetCustomerPath"], customerId); }
    }

    public interface IConfiguration
    {
        string BusinessApiBaseUrl { get; }
        string BusinessApiCustomerInvoiceDataPath { get; }
        string BusinessApiGetCustomerPath(int customerId);
    }
}