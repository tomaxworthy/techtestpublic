﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using TA.TechTest.Models.ViewModels;

namespace TA.TechTest.Business.Api.Controllers
{
    public class InvoiceController : ApiController
    {
        private readonly IInvoiceService _customerInvoiceService;

        public InvoiceController(IInvoiceService customerInvoiceService)
        {
            _customerInvoiceService = customerInvoiceService;
        }

        [HttpGet]
        [Route("customerInvoice/customerInvoiceData")]
        public JsonResult<CustomerInvoiceViewModel> GetCustomerInvoiceData()
        {
            return Json(_customerInvoiceService.GetCustomerInvoiceData());
        }
    }
}
