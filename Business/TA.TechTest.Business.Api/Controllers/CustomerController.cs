﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using TA.TechTest.Models.ViewModels;

namespace TA.TechTest.Business.Api.Controllers
{
    public class CustomerController : ApiController
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        [Route("customer/get/{customerId}")]
        public JsonResult<CustomerViewModel> GetCustomerInvoiceData(int customerId)
        {
            var customer = _customerService.GetCustomer(customerId);
            return Json(customer);
        }
    }
}
