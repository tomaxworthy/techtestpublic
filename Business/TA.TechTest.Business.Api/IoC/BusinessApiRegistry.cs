﻿using RestSharp;
using StructureMap.Configuration.DSL;
using StructureMap.Graph;
using TA.TechTest.Business.Configuration;

namespace TA.TechTest.Business.Api.IoC {
    public class BusinessApiRegistry : Registry {
        public BusinessApiRegistry()
        {
            Configure();
        }

        public BusinessApiRegistry(IConfiguration configuration)
        {
            For<IRestClient>().Use(new RestClient(configuration.DomainBaseUrl));

            For<IInvoiceService>().Use<InvoiceService>();
            For<ICustomerService>().Use<CustomerService>();
            For<IConfiguration>().Use<Configuration.Configuration>();
        }

        private void Configure() {
            Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.WithDefaultConventions();
            });
        }

    }
}