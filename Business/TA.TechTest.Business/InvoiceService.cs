﻿using System.Collections.Generic;
using RestSharp;
using TA.TechTest.Business.Configuration;
using TA.TechTest.Models.BusinessModels;
using TA.TechTest.Models.DomainModels;
using TA.TechTest.Models.ViewModels;
using TA.TechTest.RestTools;

namespace TA.TechTest.Business
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IRestClient _restClient;
        private readonly IConfiguration _configuration;

        public InvoiceService(IRestClient restClient, IConfiguration configuration)
        {
            _restClient = restClient;
            _configuration = configuration;
        }

        public CustomerInvoiceViewModel GetCustomerInvoiceData()
        {
            var topLevelSummaryRequest = new JsonRestRequest(_configuration.DomainTopLevelSummaryPath, Method.GET);
            var customerInvoiceDataRequest = new JsonRestRequest(_configuration.DomainCustomerInvoiceDataPath, Method.GET);

            var topLevelSummaryResponse = _restClient.Execute<TopLevelSummary>(topLevelSummaryRequest);
            var customerInvoiceDataResponse = _restClient.Execute<List<CustomerInvoiceData>>(customerInvoiceDataRequest);

            var viewModel = new CustomerInvoiceViewModel
            {
                CustomerInvoiceData = customerInvoiceDataResponse.Data,
                TopLevelSummary = topLevelSummaryResponse.Data
            };

            return viewModel;
        }
    }
}