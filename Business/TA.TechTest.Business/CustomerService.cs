﻿using RestSharp;
using TA.TechTest.Business.Configuration;
using TA.TechTest.Models.DomainModels;
using TA.TechTest.Models.Mappers;
using TA.TechTest.Models.ViewModels;
using TA.TechTest.RestTools;

namespace TA.TechTest.Business
{
    public class CustomerService : ICustomerService
    {
        private readonly IRestClient _restClient;
        private readonly IConfiguration _configuration;

        public CustomerService(IRestClient restClient, IConfiguration configuration)
        {
            _restClient = restClient;
            _configuration = configuration;
        }

        public CustomerViewModel GetCustomer(int customerId)
        {
            var getCustomerRequest = new JsonRestRequest(_configuration.DomainGetCustomerPath(customerId), Method.GET);

            var getCustomerResponse = _restClient.Execute<Customers>(getCustomerRequest);

            return MapCustomersToCustomerViewModel.Map(getCustomerResponse.Data);
        }
    }
}