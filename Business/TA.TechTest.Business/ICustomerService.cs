﻿using TA.TechTest.Models.ViewModels;

namespace TA.TechTest.Business
{
    public interface ICustomerService
    {
        CustomerViewModel GetCustomer(int customerId);
    }
}