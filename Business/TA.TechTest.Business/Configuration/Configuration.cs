﻿using System.Configuration;

namespace TA.TechTest.Business.Configuration
{
    public class Configuration : IConfiguration
    {
        public string DomainBaseUrl { get { return ConfigurationManager.AppSettings["DomainBaseUrl"]; } }
        public string DomainCustomerInvoiceDataPath { get { return ConfigurationManager.AppSettings["DomainCustomerInvoiceDataPath"]; } }
        public string DomainTopLevelSummaryPath { get { return ConfigurationManager.AppSettings["DomainTopLevelSummaryPath"]; } }
        public string DomainGetCustomerPath(int customerId)  { return string.Format(ConfigurationManager.AppSettings["DomainGetCustomerPath"], customerId); } 
    }

    public interface IConfiguration
    {
        string DomainBaseUrl { get; }
        string DomainCustomerInvoiceDataPath { get; }
        string DomainTopLevelSummaryPath { get; }
        string DomainGetCustomerPath(int customerId);
    }
}