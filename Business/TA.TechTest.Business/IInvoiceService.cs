﻿using TA.TechTest.Models.ViewModels;

namespace TA.TechTest.Business
{
    public interface IInvoiceService
    {
        CustomerInvoiceViewModel GetCustomerInvoiceData();
    }
}