﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestSharp;
using TA.TechTest.Business.Configuration;
using TA.TechTest.Models.BusinessModels;
using TA.TechTest.Models.DomainModels;
using TA.TechTest.Models.TestData;

namespace TA.TechTest.Business.Tests
{
    [TestClass]
    public class InvoiceServiceTests
    {
        private Mock<IRestClient> _restClient;
        private Mock<IRestResponse<TopLevelSummary>> _summaryRestResponse;
        private Mock<IRestResponse<List<CustomerInvoiceData>>> _invoiceRestResponse;
        private Mock<IConfiguration> _configuration;

        [TestMethod]
        public void InvoiceService_GetCustomerInvoiceDataWithResponse_ReturnsPopulatedObjects()
        {
            _restClient = new Mock<IRestClient>();
            _summaryRestResponse = new Mock<IRestResponse<TopLevelSummary>>();
            _summaryRestResponse.Setup(x => x.Data).Returns(TestObjects.TopLevelSummary);

            _invoiceRestResponse = new Mock<IRestResponse<List<CustomerInvoiceData>>>();
            _invoiceRestResponse.Setup(x => x.Data).Returns(new List<CustomerInvoiceData>{TestObjects.CustomerInvoiceData});

            _configuration = new Mock<IConfiguration>();
            _restClient.Setup(x => x.Execute<TopLevelSummary>(It.IsAny<IRestRequest>())).Returns(_summaryRestResponse.Object);
            _restClient.Setup(x => x.Execute<List<CustomerInvoiceData>>(It.IsAny<IRestRequest>())).Returns(_invoiceRestResponse.Object);

            var sut = new InvoiceService(_restClient.Object, _configuration.Object);

            var customer = sut.GetCustomerInvoiceData();

            Assert.AreEqual(1, customer.CustomerInvoiceData.Count);
            Assert.AreEqual(TestObjects.TopLevelSummary.PaidInvoicesMonetaryAmount, customer.TopLevelSummary.PaidInvoicesMonetaryAmount);
            Assert.AreEqual(TestObjects.TopLevelSummary.PaidInvoicesCount, customer.TopLevelSummary.PaidInvoicesCount);
        }

        [TestMethod]
        public void InvoiceService_GetCustomerInvoiceDataWithNullResponse_ReturnsNull()
        {
            _restClient = new Mock<IRestClient>();
            _summaryRestResponse = new Mock<IRestResponse<TopLevelSummary>>();
            _summaryRestResponse.Setup(x => x.Data).Returns((TopLevelSummary) null);

            _invoiceRestResponse = new Mock<IRestResponse<List<CustomerInvoiceData>>>();
            _invoiceRestResponse.Setup(x => x.Data).Returns((List<CustomerInvoiceData>)null);

            _configuration = new Mock<IConfiguration>();
            _restClient.Setup(x => x.Execute<TopLevelSummary>(It.IsAny<IRestRequest>())).Returns(_summaryRestResponse.Object);
            _restClient.Setup(x => x.Execute<List<CustomerInvoiceData>>(It.IsAny<IRestRequest>())).Returns(_invoiceRestResponse.Object);

            var sut = new InvoiceService(_restClient.Object, _configuration.Object);

            var customer = sut.GetCustomerInvoiceData();

            Assert.IsNull(customer.CustomerInvoiceData);
            Assert.IsNull(customer.TopLevelSummary);
        }
    }
}
