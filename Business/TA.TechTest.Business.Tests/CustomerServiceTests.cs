﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestSharp;
using TA.TechTest.Business.Configuration;
using TA.TechTest.Models.DomainModels;
using TA.TechTest.Models.TestData;

namespace TA.TechTest.Business.Tests
{
    [TestClass]
    public class CustomerServiceTests
    {
        private Mock<IRestClient> _restClient;
        private Mock<IRestResponse<Customers>> _restResponse;
        private Mock<IConfiguration> _configuration;

        [TestMethod]
        public void CustomerService_GetCustomerWithResponse_ReturnsCustomer()
        {
            _restClient = new Mock<IRestClient>();
            _restResponse = new Mock<IRestResponse<Customers>>();
            _restResponse.Setup(x => x.Data).Returns(TestObjects.Customer1);
            _configuration = new Mock<IConfiguration>();
            _restClient.Setup(x => x.Execute<Customers>(It.IsAny<IRestRequest>())).Returns(_restResponse.Object);

            var sut = new CustomerService(_restClient.Object, _configuration.Object);

            var customer = sut.GetCustomer(TestObjects.Customer1.CustomerId);

            Assert.AreEqual(TestObjects.Customer1.CustomerId, customer.CustomerId);
            Assert.AreEqual(TestObjects.Customer1.Address1, customer.Address1);
            Assert.AreEqual(TestObjects.Customer1.Address2, customer.Address2);
            Assert.AreEqual(TestObjects.Customer1.Name, customer.Name);
            Assert.AreEqual(TestObjects.Customer1.Postcode, customer.Postcode);
            Assert.AreEqual(TestObjects.Customer1.Telephone, customer.Telephone);
        }

        [TestMethod]
        public void CustomerService_GetCustomerNoResponse_ReturnsEmptyCustomer()
        {
            _restClient = new Mock<IRestClient>();
            _restResponse = new Mock<IRestResponse<Customers>>();
            _restResponse.Setup(x => x.Data).Returns((Customers) null);
            _configuration = new Mock<IConfiguration>();
            _restClient.Setup(x => x.Execute<Customers>(It.IsAny<IRestRequest>())).Returns(_restResponse.Object);

            var sut = new CustomerService(_restClient.Object, _configuration.Object);

            var customer = sut.GetCustomer(TestObjects.Customer1.CustomerId);

            Assert.IsNull(customer);
        }
    }
}
