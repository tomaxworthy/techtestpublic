﻿using RestSharp.Deserializers;
using RestSharp.Serializers;

namespace TA.TechTest.RestTools
{
    public interface IJsonSerializer : ISerializer, IDeserializer
    {

    }
}
