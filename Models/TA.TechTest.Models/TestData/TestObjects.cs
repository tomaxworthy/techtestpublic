﻿using System;
using TA.TechTest.Models.BusinessModels;
using TA.TechTest.Models.DomainModels;

namespace TA.TechTest.Models.TestData
{
    public static class TestObjects
    {
        public static Invoices Customer1PaidInvoice1 = new Invoices
        {
            CustomerId = 123,
            InvoiceDate = DateTime.Now.Subtract(new TimeSpan(2, 0, 0, 1)),
            InvoiceId = 1,
            IsPaid = true,
            Ref = "Post Its",
            Value = 6.25m
        };

        public static Invoices Customer1UnpaidInvoice2 = new Invoices
        {
            CustomerId = 123,
            InvoiceDate = DateTime.Now.Subtract(new TimeSpan(2, 0, 0)),
            InvoiceId = 2,
            IsPaid = false,
            Ref = "Pens",
            Value = 4.50m
        };

        public static Invoices Customer1UnpaidInvoice3 = new Invoices
        {
            CustomerId = 123,
            InvoiceDate = DateTime.Now.Subtract(new TimeSpan(3, 0, 0)),
            InvoiceId = 3,
            IsPaid = false,
            Ref = "Paper",
            Value = 12.50m
        };

        public static Invoices Customer2PaidInvoice1 = new Invoices
        {
            CustomerId = 456,
            InvoiceDate = DateTime.Now.Subtract(new TimeSpan(1, 0, 0)),
            InvoiceId = 4,
            IsPaid = true,
            Ref = "Consultants",
            Value = 60000m
        };

        public static Invoices Customer2PaidInvoice2 = new Invoices
        {
            CustomerId = 456,
            InvoiceDate = DateTime.Now,
            InvoiceId = 5,
            IsPaid = true,
            Ref = "Testing",
            Value = 70000m
        };

        public static Customers Customer1 = new Customers
        {
            CustomerId = 123,
            Address1 = "123 the road",
            Address2 = "Southampton",
            Name = "CustomerOne",
            Postcode = "SO196RR",
            Telephone = "02836555444"
        };

        public static Customers Customer2 = new Customers
        {
            CustomerId = 456,
            Address1 = "22 the avenue",
            Address2 = "Winchester",
            Name = "CustomerTwo",
            Postcode = "SO93SF",
            Telephone = "0283408876"
        };

        public static TopLevelSummary TopLevelSummary = new TopLevelSummary
        {
            PaidInvoicesMonetaryAmount = 8790428498432m,
            PaidInvoicesCount = 5310
        };

        public static CustomerInvoiceData CustomerInvoiceData = new CustomerInvoiceData
        {
            CustomerId = 123,
            MostRecentInvoiceAmount = 7655.00m,
            MostRecentInvoiceRef = "Ref",
            Name = "Test Customer",
            OutstandingInvoiceCount = 3,
            PaidInvoicesMonetaryTotal = 33500.66m,
            OutstandingInvoiceMonetaryTotal = 4035.75m
        };
    }
}
