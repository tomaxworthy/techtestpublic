﻿using System;
using TA.TechTest.Models.DomainModels;
using TA.TechTest.Models.ViewModels;

namespace TA.TechTest.Models.Mappers
{
    public static class MapCustomersToCustomerViewModel
    {
        public static CustomerViewModel Map(Customers customer)
        {
            if (customer == null)
                return null;

            return new CustomerViewModel
            {
                Address1 = customer.Address1,
                Address2 = customer.Address2,
                CustomerId = customer.CustomerId,
                Name = customer.Name,
                Postcode = customer.Postcode,
                Telephone = customer.Telephone
            };
        }
    }
}
