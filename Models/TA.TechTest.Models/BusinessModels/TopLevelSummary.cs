﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TA.TechTest.Models.DomainModels
{
    public class TopLevelSummary
    {
        [JsonProperty(PropertyName = "paidInvoicesCount")]
        [DisplayName("Paid Invoices Count")]
        public int PaidInvoicesCount { get; set; }

        [JsonProperty(PropertyName = "paidInvoicesMonetaryAmount")]
        [DisplayName("Paid Invoices Monetary Amount")]
        [DisplayFormat(DataFormatString = "{0:C0}")]
        public decimal PaidInvoicesMonetaryAmount { get; set; }
    }
}
