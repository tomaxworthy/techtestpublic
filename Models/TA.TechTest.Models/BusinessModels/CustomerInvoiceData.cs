﻿using Newtonsoft.Json;

namespace TA.TechTest.Models.BusinessModels
{
    public class CustomerInvoiceData
    {
        [JsonProperty(PropertyName = "customerId")]
        public int CustomerId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "mostRecentInvoiceRef")]
        public string MostRecentInvoiceRef { get; set; }

        [JsonProperty(PropertyName = "mostRecentInvoiceAmount")]
        public decimal MostRecentInvoiceAmount { get; set; }

        [JsonProperty(PropertyName = "outstandingInvoiceCount")]
        public int OutstandingInvoiceCount { get; set; }

        [JsonProperty(PropertyName = "outstandingInvoiceMonetaryTotal")]
        public decimal OutstandingInvoiceMonetaryTotal { get; set; }

        [JsonProperty(PropertyName = "paidInvoicesMonetaryTotal")]
        public decimal PaidInvoicesMonetaryTotal { get; set; }
    }
}
