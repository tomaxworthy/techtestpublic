﻿using System;
using System.Data.Linq.Mapping;

namespace TA.TechTest.Models.DomainModels
{
    [Table(Name = "Invoices")]
    public class Invoices
    {
        [Column(Name= "InvoiceId")]
        public int InvoiceId { get; set; }

        [Column(Name = "CustomerId")]
        public int CustomerId { get; set; }

        [Column(Name = "Ref")]
        public string Ref { get; set; }

        [Column(Name = "InvoiceDate")]
        public DateTime InvoiceDate { get; set; }

        [Column(Name = "IsPaid")]
        public bool IsPaid { get; set; }

        [Column(Name = "Value")]
        public decimal Value { get; set; }
    }
}