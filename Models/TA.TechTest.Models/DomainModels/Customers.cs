﻿using System.Data.Linq.Mapping;

namespace TA.TechTest.Models.DomainModels
{
    [Table(Name = "Customers")]
    public class Customers
    {
        [Column(Name= "CustomerId")]
        public int CustomerId { get; set; }

        [Column(Name = "Name")]
        public string Name { get; set; }

        [Column(Name = "Address1")]
        public string Address1 { get; set; }

        [Column(Name = "Address2")]
        public string Address2 { get; set; }

        [Column(Name = "Postcode")]
        public string Postcode { get; set; }

        [Column(Name = "Telephone")]
        public string Telephone { get; set; }
    }
}