﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TA.TechTest.Models.BusinessModels;
using TA.TechTest.Models.DomainModels;

namespace TA.TechTest.Models.ViewModels
{
    public class CustomerInvoiceViewModel
    {
        [JsonProperty(PropertyName = "topLevelSummary")]
        public TopLevelSummary TopLevelSummary { get; set; }
        [JsonProperty(PropertyName = "customerInvoiceData")]
        public List<CustomerInvoiceData> CustomerInvoiceData { get; set; }
    }
}
