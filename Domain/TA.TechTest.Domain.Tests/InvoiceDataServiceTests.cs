﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TA.TechTest.Models.DomainModels;
using TA.TechTest.Models.TestData;

namespace TA.TechTest.Domain.Tests
{
    [TestClass]
    public class InvoiceDataServiceTests
    {
        Mock<ICustomerRepository> _customerRepository;
        ICustomerDataService _customerDataService;
        Mock<IInvoiceRepository> _invoiceRepository;

        private void SetupRepositoriesAndService(IQueryable<Invoices> queryableInvoices, IQueryable<Customers> queryableCustomers)
        {
            _customerRepository = new Mock<ICustomerRepository>();
            _customerRepository.Setup(x => x.GetCustomers()).Returns(queryableCustomers);

            _customerDataService = new CustomerDataService(_customerRepository.Object);
            _invoiceRepository = new Mock<IInvoiceRepository>();
            _invoiceRepository.Setup(x => x.GetInvoices()).Returns(queryableInvoices);
        }

        [TestMethod]
        public void InvoiceDataService_NoInvoices_ReturnsNoCustomerInvoices()
        {
            IQueryable<Invoices> queryableInvoices = new EnumerableQuery<Invoices>(new List<Invoices> {  });
            IQueryable<Customers> queryableCustomers = new EnumerableQuery<Customers>(new List<Customers> { TestObjects.Customer1 });

            SetupRepositoriesAndService(queryableInvoices, queryableCustomers);
                
            var sut = new InvoiceDataService(_customerDataService, _invoiceRepository.Object);
            var totalSummary = sut.TotalInvoiceDetails();

            Assert.AreEqual(0, sut.InvoiceData().Count());
            Assert.AreEqual(0,totalSummary.PaidInvoicesMonetaryAmount);
            Assert.AreEqual(0,totalSummary.PaidInvoicesCount);
        }

        [TestMethod]
        public void InvoiceDataService_NoCustomers_ReturnsEmptyList()
        {
            IQueryable<Invoices> queryableInvoices = new EnumerableQuery<Invoices>(new List<Invoices> { TestObjects.Customer1PaidInvoice1, TestObjects.Customer1UnpaidInvoice2 });
            IQueryable<Customers> queryableCustomers = new EnumerableQuery<Customers>(new List<Customers> {  });

            SetupRepositoriesAndService(queryableInvoices, queryableCustomers);

            var sut = new InvoiceDataService(_customerDataService, _invoiceRepository.Object);
            var invoice = sut.InvoiceData();

            Assert.AreEqual(0,invoice.Count());
        }

        [TestMethod]
        public void InvoiceDataService_CustomerUnpaidInvoices_ReturnsCorrectTotal()
        {
            IQueryable<Invoices> queryableInvoices = new EnumerableQuery<Invoices>(new List<Invoices> { TestObjects.Customer1PaidInvoice1, TestObjects.Customer1UnpaidInvoice2, TestObjects.Customer1UnpaidInvoice3 });
            IQueryable<Customers> queryableCustomers = new EnumerableQuery<Customers>(new List<Customers> { TestObjects.Customer1 });

            SetupRepositoriesAndService(queryableInvoices, queryableCustomers);

            var sut = new InvoiceDataService(_customerDataService, _invoiceRepository.Object);
            var invoice = sut.InvoiceData().First();

            Assert.AreEqual(TestObjects.Customer1.Name, invoice.Name);
            Assert.AreEqual(TestObjects.Customer1UnpaidInvoice2.Value + TestObjects.Customer1UnpaidInvoice3.Value, invoice.OutstandingInvoiceMonetaryTotal);
            Assert.AreEqual(2, invoice.OutstandingInvoiceCount);
        }

        [TestMethod]
        public void InvoiceDataService_CustomerMostRecentInvoice_ReturnsMostRecentByDate()
        {
            IQueryable<Invoices> queryableInvoices = new EnumerableQuery<Invoices>(new List<Invoices> { TestObjects.Customer1PaidInvoice1, TestObjects.Customer1UnpaidInvoice2 });
            IQueryable<Customers> queryableCustomers = new EnumerableQuery<Customers>(new List<Customers> { TestObjects.Customer1 });

            SetupRepositoriesAndService(queryableInvoices, queryableCustomers);

            var sut = new InvoiceDataService(_customerDataService, _invoiceRepository.Object);
            var invoice = sut.InvoiceData().First();

            Assert.AreEqual(TestObjects.Customer1.Name, invoice.Name);
            Assert.AreEqual(TestObjects.Customer1UnpaidInvoice2.Value, invoice.MostRecentInvoiceAmount);
            Assert.AreEqual(TestObjects.Customer1UnpaidInvoice2.Ref, invoice.MostRecentInvoiceRef);
        }

        [TestMethod]
        public void InvoiceDataService_CustomerPaidInvoices_ReturnsOnlyPaidInvoices()
        {
            IQueryable<Invoices> queryableInvoices = new EnumerableQuery<Invoices>(new List<Invoices> { TestObjects.Customer1PaidInvoice1, TestObjects.Customer1UnpaidInvoice2, TestObjects.Customer1UnpaidInvoice3, TestObjects.Customer2PaidInvoice1, TestObjects.Customer2PaidInvoice2 });
            IQueryable<Customers> queryableCustomers = new EnumerableQuery<Customers>(new List<Customers> { TestObjects.Customer1, TestObjects.Customer2 });

            SetupRepositoriesAndService(queryableInvoices, queryableCustomers);

            var sut = new InvoiceDataService(_customerDataService, _invoiceRepository.Object);
            var totalSummary = sut.TotalInvoiceDetails();
            var invoice = sut.InvoiceData().ElementAt(0);
            var invoice2 = sut.InvoiceData().ElementAt(1);

            Assert.AreEqual(TestObjects.Customer1PaidInvoice1.Value + TestObjects.Customer2PaidInvoice1.Value + TestObjects.Customer2PaidInvoice2.Value, totalSummary.PaidInvoicesMonetaryAmount);
            Assert.AreEqual(3, totalSummary.PaidInvoicesCount);

            Assert.AreEqual(TestObjects.Customer1.Name, invoice.Name);
            Assert.AreEqual(TestObjects.Customer1PaidInvoice1.Value, invoice.PaidInvoicesMonetaryTotal);

            Assert.AreEqual(TestObjects.Customer2.Name, invoice2.Name);
            Assert.AreEqual(TestObjects.Customer2PaidInvoice1.Value + TestObjects.Customer2PaidInvoice2.Value, invoice2.PaidInvoicesMonetaryTotal);
        }
    }
}
