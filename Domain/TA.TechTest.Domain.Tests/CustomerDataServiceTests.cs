﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TA.TechTest.Models.DomainModels;
using TA.TechTest.Models.TestData;

namespace TA.TechTest.Domain.Tests
{
    [TestClass]
    public class CustomerDataServiceTests
    {
        Mock<ICustomerRepository> _customerRepository;

        private void SetupRepositoriesAndService(IQueryable<Customers> queryableCustomers)
        {
            _customerRepository = new Mock<ICustomerRepository>();
            _customerRepository.Setup(x => x.GetCustomers()).Returns(queryableCustomers);
        }

        [TestMethod]
        public void CustomerDataService_AllCustomers_ReturnsAllCustomers()
        {
            IQueryable<Customers> queryableCustomers = new EnumerableQuery<Customers>(new List<Customers> { TestObjects.Customer1, TestObjects.Customer2 });

            SetupRepositoriesAndService(queryableCustomers);

            var sut = new CustomerDataService(_customerRepository.Object);
            var customerOne = sut.AllCustomers().ElementAt(0);
            var customerTwo = sut.AllCustomers().ElementAt(1);

            Assert.AreEqual(TestObjects.Customer1.Name, customerOne.Name);
            Assert.AreEqual(TestObjects.Customer1.Address1, customerOne.Address1);
            Assert.AreEqual(TestObjects.Customer1.Address2, customerOne.Address2);
            Assert.AreEqual(TestObjects.Customer1.CustomerId, customerOne.CustomerId);
            Assert.AreEqual(TestObjects.Customer1.Postcode, customerOne.Postcode);
            Assert.AreEqual(TestObjects.Customer1.Telephone, customerOne.Telephone);

            Assert.AreEqual(TestObjects.Customer2.Name, customerTwo.Name);
            Assert.AreEqual(TestObjects.Customer2.Address1, customerTwo.Address1);
            Assert.AreEqual(TestObjects.Customer2.Address2, customerTwo.Address2);
            Assert.AreEqual(TestObjects.Customer2.CustomerId, customerTwo.CustomerId);
            Assert.AreEqual(TestObjects.Customer2.Postcode, customerTwo.Postcode);
            Assert.AreEqual(TestObjects.Customer2.Telephone, customerTwo.Telephone);
        }

        [TestMethod]
        public void CustomerDataService_GetCustomer_ReturnsCorrectCustomerById()
        {
            IQueryable<Customers> queryableCustomers = new EnumerableQuery<Customers>(new List<Customers> { TestObjects.Customer1, TestObjects.Customer2 });

            SetupRepositoriesAndService(queryableCustomers);

            var sut = new CustomerDataService(_customerRepository.Object);
            var customerOne = sut.GetCustomer(TestObjects.Customer1.CustomerId);

            Assert.AreEqual(TestObjects.Customer1.Name, customerOne.Name);
            Assert.AreEqual(TestObjects.Customer1.Address1, customerOne.Address1);
            Assert.AreEqual(TestObjects.Customer1.Address2, customerOne.Address2);
            Assert.AreEqual(TestObjects.Customer1.CustomerId, customerOne.CustomerId);
            Assert.AreEqual(TestObjects.Customer1.Postcode, customerOne.Postcode);
            Assert.AreEqual(TestObjects.Customer1.Telephone, customerOne.Telephone);
        }

        [TestMethod]
        public void CustomerDataService_GetCustomerWrongId_ReturnsNull()
        {
            IQueryable<Customers> queryableCustomers = new EnumerableQuery<Customers>(new List<Customers> { TestObjects.Customer1 });

            SetupRepositoriesAndService(queryableCustomers);

            var sut = new CustomerDataService(_customerRepository.Object);
            var customerOne = sut.GetCustomer(TestObjects.Customer2.CustomerId);

            Assert.IsNull(customerOne);
        }

        [TestMethod]
        public void CustomerDataService_GetAllCustomersNone_ReturnsEmpty()
        {
            IQueryable<Customers> queryableCustomers = new EnumerableQuery<Customers>(new List<Customers> {  });

            SetupRepositoriesAndService(queryableCustomers);

            var sut = new CustomerDataService(_customerRepository.Object);
            var customers = sut.AllCustomers();

            Assert.AreEqual(0, customers.Count());
        }

        [TestMethod]
        public void CustomerDataService_GetCustomerNone_ReturnsNull()
        {
            IQueryable<Customers> queryableCustomers = new EnumerableQuery<Customers>(new List<Customers> { });

            SetupRepositoriesAndService(queryableCustomers);

            var sut = new CustomerDataService(_customerRepository.Object);
            var customers = sut.GetCustomer(TestObjects.Customer1.CustomerId);

            Assert.IsNull(customers);
        }
    }
}
