﻿using System.Configuration;

namespace TA.TechTest.Domain.Configuration
{
    public class Configuration : IConfiguration
    {
        public string DatabaseConnection { get { return ConfigurationManager.AppSettings["CustomerInvoiceDatabaseConnectionString"]; } }
    }

    public interface IConfiguration
    {
        string DatabaseConnection { get; }
    }
}