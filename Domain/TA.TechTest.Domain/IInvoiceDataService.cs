﻿using System.Collections.Generic;
using TA.TechTest.Models.BusinessModels;
using TA.TechTest.Models.DomainModels;

namespace TA.TechTest.Domain
{
    public interface IInvoiceDataService
    {
        TopLevelSummary TotalInvoiceDetails();
        IEnumerable<CustomerInvoiceData> InvoiceData();
    }
}