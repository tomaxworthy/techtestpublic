﻿using System.Linq;
using TA.TechTest.Models.DomainModels;

namespace TA.TechTest.Domain
{
    public interface ICustomerRepository
    {
        IQueryable<Customers> GetCustomers();
    }
}