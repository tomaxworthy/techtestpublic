﻿namespace TA.TechTest.Domain
{
    public enum InvoiceQueryStatus
    {
        Paid,
        Unpaid,
        All
    }
}