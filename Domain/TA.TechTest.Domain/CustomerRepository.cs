﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TA.TechTest.Models.DomainModels;

namespace TA.TechTest.Domain
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DataContext _dataContext;

        public CustomerRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IQueryable<Customers> GetCustomers()
        {
            return _dataContext.GetTable<Customers>();

        }
    }
}

