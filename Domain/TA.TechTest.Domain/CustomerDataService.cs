﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TA.TechTest.Models.DomainModels;

namespace TA.TechTest.Domain
{
    public class CustomerDataService : ICustomerDataService
    {
        private readonly ICustomerRepository _customerRepository;
        public CustomerDataService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public Customers GetCustomer(int customerId)
        {
            var customersTable = _customerRepository.GetCustomers();

            var customer = customersTable.FirstOrDefault(c => c.CustomerId == customerId);

            return customer;
        }

        public IQueryable<Customers> AllCustomers()
        {
            var customers = _customerRepository.GetCustomers();

            return customers;
        }
    }

    public interface ICustomerDataService
    {
        Customers GetCustomer(int customerId);
        IQueryable<Customers> AllCustomers();
    }
}
