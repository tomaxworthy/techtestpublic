﻿using System.Collections.Generic;
using System.Linq;
using TA.TechTest.Models.DomainModels;

namespace TA.TechTest.Domain
{
    public interface IInvoiceRepository
    {
        IQueryable<Invoices> GetInvoices();
    }
}