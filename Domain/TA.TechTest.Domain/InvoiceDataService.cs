﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TA.TechTest.Models.BusinessModels;
using TA.TechTest.Models.DomainModels;

namespace TA.TechTest.Domain
{
    public class InvoiceDataService : IInvoiceDataService
    {
        private readonly ICustomerDataService _customerDataService;
        private readonly IInvoiceRepository _invoiceRepository;

        public InvoiceDataService(ICustomerDataService customerDataService, IInvoiceRepository invoiceRepository)
        {
            _customerDataService = customerDataService;
            _invoiceRepository = invoiceRepository;
        }

        public TopLevelSummary TotalInvoiceDetails()
        {
            return CalculateTotalInvoiceSummary(_invoiceRepository.GetInvoices());
        }

        private TopLevelSummary CalculateTotalInvoiceSummary(IQueryable<Invoices> invoiceTable)
        {
            var topLevelSummary = new TopLevelSummary();
            var invoicesPaid = invoiceTable.Where(i => i.IsPaid);

            topLevelSummary.PaidInvoicesCount = invoicesPaid.Count();
            topLevelSummary.PaidInvoicesMonetaryAmount = invoicesPaid.Any() ? invoicesPaid.Sum(i => i.Value): decimal.Zero;

            return topLevelSummary;
        }

        public IEnumerable<CustomerInvoiceData> InvoiceData()
        {
            var customers = _customerDataService.AllCustomers();


            var customerInvoices = (from invoice in _invoiceRepository.GetInvoices()
                group invoice by new { invoice.CustomerId }
                into grp
                join cust in customers on grp.Key.CustomerId equals cust.CustomerId
                orderby cust.Name
                select new CustomerInvoiceData
                {
                    CustomerId = grp.Key.CustomerId,
                    Name = cust.Name,
                    PaidInvoicesMonetaryTotal = grp.Where(i=>i.IsPaid).Sum(i => i.Value),
                    OutstandingInvoiceMonetaryTotal = grp.Where(i=>!i.IsPaid).Sum(i => i.Value),
                    MostRecentInvoiceAmount = grp.OrderByDescending(i=>i.InvoiceDate).First().Value,
                    MostRecentInvoiceRef = grp.OrderByDescending(i => i.InvoiceDate).First().Ref,
                    OutstandingInvoiceCount = grp.Count(i => !i.IsPaid)
                }).ToList();

            return customerInvoices;
        }
    }
}
