﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using TA.TechTest.Models.BusinessModels;
using TA.TechTest.Models.DomainModels;

namespace TA.TechTest.Domain
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private readonly DataContext _dataContext;

        public InvoiceRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IQueryable<Invoices> GetInvoices()
        {
            return _dataContext.GetTable<Invoices>();
        }
    }
}
