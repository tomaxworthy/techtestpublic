﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using TA.TechTest.Models.BusinessModels;
using TA.TechTest.Models.DomainModels;

namespace TA.TechTest.Domain.Api.Controllers
{
    public class CustomerController : ApiController
    {
        private readonly ICustomerDataService _customerDataService;

        public CustomerController(ICustomerDataService customerDataService)
        {
            _customerDataService = customerDataService;
        }

        [HttpGet]
        [Route("customer/get/{customerId}")]
        public JsonResult<Customers> GetCustomer(int customerId)
        {
            var customerData = _customerDataService.GetCustomer(customerId);
            return Json(customerData);
        }
    }
}
