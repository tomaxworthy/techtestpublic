﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using TA.TechTest.Models.BusinessModels;
using TA.TechTest.Models.DomainModels;

namespace TA.TechTest.Domain.Api.Controllers
{
    public class CustomerInvoiceController : ApiController
    {
        private readonly IInvoiceDataService _customerInvoiceDataService;

        public CustomerInvoiceController(IInvoiceDataService customerInvoiceDataService)
        {
            _customerInvoiceDataService = customerInvoiceDataService;
        }

        [HttpGet]
        [Route("customerInvoice/customerInvoiceData")]
        public JsonResult<IEnumerable<CustomerInvoiceData>> GetCustomerInvoiceData()
        {
            var customerInvoiceData = _customerInvoiceDataService.InvoiceData();
            return Json(customerInvoiceData);
        }

        [HttpGet]
        [Route("customerInvoice/topLevelSummary")]
        public JsonResult<TopLevelSummary> GetTopLevelSummary()
        {
            var topLevelSummary = _customerInvoiceDataService.TotalInvoiceDetails();
            return Json(topLevelSummary);
        }
    }
}
