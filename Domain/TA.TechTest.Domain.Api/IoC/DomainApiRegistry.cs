﻿using System.Data.Linq;
using StructureMap;
using StructureMap.Configuration.DSL;
using StructureMap.Graph;
using TA.TechTest.Domain.Configuration;

namespace TA.TechTest.Domain.Api.IoC {
    public class DomainApiRegistry : Registry {
        public DomainApiRegistry() {
            Configure();
        }

        public DomainApiRegistry(IConfiguration configuration)
        {
            var sharedDataContext = new DataContext(configuration.DatabaseConnection);

            For<ICustomerRepository>().Use(new CustomerRepository(sharedDataContext));
            For<IInvoiceRepository>().Use(new InvoiceRepository(sharedDataContext));
            For<IInvoiceDataService>().Use<InvoiceDataService>();
            For<ICustomerDataService>().Use<CustomerDataService>();
            Configure();
        }

        private void Configure() {
            Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.WithDefaultConventions();
            });
        }

    }
}